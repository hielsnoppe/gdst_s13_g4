/*
 * Example code used in exercises for lecture "Grundlagen des Software-Testens"
 * Created and given by Ina Schieferdecker and Edzard Höfig
 * Freie Universität Berlin
 */
package exercise1.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import exercise1.addressbook.model.AddressBook;
import exercise1.addressbook.model.EmailAddress;
import exercise1.addressbook.model.Entry;
import exercise1.addressbook.model.Gender;
import exercise1.addressbook.model.PhoneNumber;
import exercise1.addressbook.model.SizeLimitReachedException;

/**
 * Uebung 1 - Grundlagen des Testens mit JUnit
 * 
 * Bitte Nummer der Gruppe eintragen:
 * 4
 * 
 * Bitte Gruppenmitglieder eintragen:
 * @author Hoppe, Niels
 * @author Junker, Severin
 * @author Malotky, Troels
 * @author Neumann, Daniel
 */
public class AddressBookFunctionalTest {
	
	// The component under test
	private AddressBook addressBook;
	
	/*
	 * Aufgabe 3a)
	 * Schreiben Sie eine Methode zum Aufsetzen der Testumgebung ("Fixture").
	 * Diese Methode soll automatisch vor jedem einzelnen JUnit Testfall ausgeführt werden.
	 * Innerhalb der Methode soll mindestens ein neues AddressBook Objekt angelegt und im Attribut "addressBook" gepeichert werden.
	 */
	
	@Before
	public void setUp ()
	{
		this.addressBook = new AddressBook();
	}
	
	/*
	 * Aufgabe 3b)
	 * Schreiben Sie einen JUnit Testfall zum überprüfen der Funktionalität der addEntry() Methode.
	 */

	@Test
	public void addEntry ()
	{
		boolean success;
		Entry entry;
		
		/*
		 * Try adding an entry.
		 * Assert it is accepted.
		 */
		entry = new Entry("Duck", "Daisy", Gender.Female, new PhoneNumber(1));
		try {
			success = this.addressBook.addEntry(entry);
			assertTrue(success);
		} catch (SizeLimitReachedException e) {
			assertEquals(AddressBook.sizeLimit, 0);
		}
		
		/*
		 * Try adding the same entry again.
		 * Assert it is rejected.
		 */
		try {
			success = this.addressBook.addEntry(entry);
			assertFalse(success);
		} catch (SizeLimitReachedException e) {
			assertEquals(AddressBook.sizeLimit, 0);
		}
		
		/*
		 * Try adding another entry.
		 * Assert it is accepted.
		 */
		entry = new Entry("Duck", "Donald", Gender.Male, new PhoneNumber(2));
		try {
			success = this.addressBook.addEntry(entry);
			assertTrue(success);
		} catch (SizeLimitReachedException e) {
			assertEquals(AddressBook.sizeLimit, 1);
		}
		
		/*
		 * Try to fill the book with sizeLimit more different entries.
		 * Assert the first sizeLimit - 2 entries are accepted
		 * and the last 2 excess entries throw an appropriate exception.
		 */
		for (int i = 3; i <= AddressBook.sizeLimit + 2; i++) {
			entry = new Entry(Integer.toString(i), Integer.toString(i), Gender.Male, new PhoneNumber(i));
			try {
				success = this.addressBook.addEntry(entry);
				assertTrue(success);
			} catch (SizeLimitReachedException e) {
				assertTrue(AddressBook.sizeLimit < i);
			}
		}
	}
	
	/*
	 * Aufgabe 3c)
	 * Schreiben Sie einen JUnit Testfall zum überprüfen der Funktionalität der getEntry() Methode.
	 */
	
	@Test
	public void getEntry ()
	{
		boolean success;
		Entry entry, result;
		
		/*
		 * Try adding an entry.
		 * Assert it is accepted.
		 */
		entry = new Entry("Duck", "Daisy", Gender.Female, new PhoneNumber(1));
		try {
			success = this.addressBook.addEntry(entry);
			assertTrue(success);
		} catch (SizeLimitReachedException e) {
			assertEquals(AddressBook.sizeLimit, 0);
		}
		
		/*
		 * Try getting the entry added before.
		 * Assert it is returned.
		 */
		result = this.addressBook.getEntry("Duck", "Daisy");
		assertEquals(entry, result);
		
		/*
		 * Try getting an entry not added before.
		 * Assert null is returned. 
		 */
		result = this.addressBook.getEntry("Duck", "Donald");
		assertNull(result);
	}
}