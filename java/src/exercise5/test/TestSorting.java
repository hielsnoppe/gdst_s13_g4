/*
 * Example code used in exercises for lecture "Grundlagen des Software-Testens"
 * Created and given by Ina Schieferdecker and Edzard Hoefig
 * Freie Universitaet Berlin, SS 2013
 */
package exercise5.test;

import javax.swing.JTable;
import javax.swing.table.TableModel;

import junit.extensions.abbot.ComponentTestFixture;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import abbot.finder.ComponentNotFoundException;
import abbot.finder.MultipleComponentsFoundException;
import abbot.finder.matchers.NameMatcher;
import abbot.tester.JButtonTester;
import abbot.tester.JTableLocation;
import abbot.tester.JTableTester;
import abbot.tester.JTextComponentTester;

/**
 * Uebung 5 - Black Box Test
 * GUI testing
 * 
 * Bitte Nummer der Gruppe eintragen:
 * 4
 * 
 * Bitte Gruppenmitglieder eintragen:
 * @author Hoppe, Niels
 * @author Junker, Severin
 * @author Graf von Malotky, Troels Nikolaj
 * @author Neumann, Daniel
 */
public class TestSorting extends ComponentTestFixture {

	// Tester for (radio) button components
	private JButtonTester buttonTester;

	// Tester for table components
	private JTableTester tableTester;

	// Tester for text field components
	private JTextComponentTester textTester;

	
	/**
	 * Creates test fixture
	 */
	@Before
	public void setUp() throws Exception {

		// Start the application
		exercise5.addressbook.Manager.main(null);
		
		// Setup some test instrumentation
		this.buttonTester = new JButtonTester();
		this.tableTester = new JTableTester();
		this.textTester = new JTextComponentTester();
	}

	/**
	 * Removes test fixture
	 */
	@After
	public void tearDown() throws Exception {
		// Nothing to do
	}
	
	/*
	 * Aufgabe 4
	 * Verwenden Sie JUnit zur Ueberpruefung der korrekten Sortierreihenfolge beim Hinzufuegen von Eintraegen in das Adressbuch.  
	 * Verwenden Sie das Abbot GUI test framework zur Testdurchfuehrung.
	 * 
	 * Hinweis:
	 * Die aktuelle Version von Abbot (1.3.0) hat auf manchen Systemen (z.B. OS X 10.8) Schwierigkeiten die richtige "Keymap" zu 
	 * erkennen. Als Folge davon werden einige Zeichen nicht richtig in die Textfelder eingetragen (z.B. Sonderzeichen, 
	 * y und z vertauscht...). Bitte ueberpruefen Sie bei Ihren Testfaellen, ob Abbot die richtigen Testdaten eintraegt und waehlen 
	 * Sie ggfs. andere.
	 */
	@Test
	public void testOrderingCorrectLastnames() throws ComponentNotFoundException, MultipleComponentsFoundException{
	    // Add new users
	    buttonTester.actionClick(getFinder().find(new NameMatcher("addButton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("firstNameTextfield")), "Philip J");
	    textTester.actionEnterText(getFinder().find(new NameMatcher("lastNameTextfield")), "Fry");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("maleRadiobutton")));
	    buttonTester.actionClick(getFinder().find(new NameMatcher("phoneRadiobutton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("contactInformationTextfield")), "999999");
	    textTester.actionEnterText(getFinder().find(new NameMatcher("dateOfBirthTextfield")), "1.1.1974");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("okButton")));
	    
	    buttonTester.actionClick(getFinder().find(new NameMatcher("addButton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("firstNameTextfield")), "Leela");
	    textTester.actionEnterText(getFinder().find(new NameMatcher("lastNameTextfield")), "Turanga");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("femaleRadiobutton")));
	    buttonTester.actionClick(getFinder().find(new NameMatcher("emailRadiobutton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("contactInformationTextfield")), "leela@futurama.com");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("okButton")));
	    
	    buttonTester.actionClick(getFinder().find(new NameMatcher("addButton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("firstNameTextfield")), "Bender B");
	    textTester.actionEnterText(getFinder().find(new NameMatcher("lastNameTextfield")), "Rodriguez");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("maleRadiobutton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("dateOfBirthTextfield")), "1.1.2998");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("okButton")));
	    
	    TableModel content = ((JTable) getFinder().find(new NameMatcher("viewTable"))).getModel();
	    assertEquals("Fry is not the first", "Fry", content.getValueAt(0, 1));
	    assertEquals("Rodriguez is not the second", "Rodriguez", content.getValueAt(1, 1));
	    assertEquals("Turanga is not the third", "Turanga", content.getValueAt(2, 1));
	}
	
	@Test
	public void testOrderingCorrectFirstnames() throws ComponentNotFoundException, MultipleComponentsFoundException{
	    // Add new users
	    buttonTester.actionClick(getFinder().find(new NameMatcher("addButton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("firstNameTextfield")), "Hubert J");
	    textTester.actionEnterText(getFinder().find(new NameMatcher("lastNameTextfield")), "Farnsworth");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("maleRadiobutton")));
	    buttonTester.actionClick(getFinder().find(new NameMatcher("okButton")));
	    
	    buttonTester.actionClick(getFinder().find(new NameMatcher("addButton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("firstNameTextfield")), "Cubert");
	    textTester.actionEnterText(getFinder().find(new NameMatcher("lastNameTextfield")), "Farnsworth");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("maleRadiobutton")));
	    buttonTester.actionClick(getFinder().find(new NameMatcher("emailRadiobutton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("contactInformationTextfield")), "farnsey@futurama.com");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("okButton")));
	    
	    TableModel content = ((JTable) getFinder().find(new NameMatcher("viewTable"))).getModel();
	    assertEquals("Cubert is not the first", "Cubert", content.getValueAt(0, 0));
	    assertEquals("Hubert J is not the second", "Hubert J", content.getValueAt(1, 0));
	}
	
	@Test
	public void testOrderingWithUmlaut() throws ComponentNotFoundException, MultipleComponentsFoundException{
	    // Add new users
	    buttonTester.actionClick(getFinder().find(new NameMatcher("addButton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("firstNameTextfield")), "Nibbler");
	    textTester.actionEnterText(getFinder().find(new NameMatcher("lastNameTextfield")), "Tääst");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("maleRadiobutton")));
	    buttonTester.actionClick(getFinder().find(new NameMatcher("okButton")));
	    
	    buttonTester.actionClick(getFinder().find(new NameMatcher("addButton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("firstNameTextfield")), "Nibbler");
	    textTester.actionEnterText(getFinder().find(new NameMatcher("lastNameTextfield")), "Täest");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("maleRadiobutton")));
	    buttonTester.actionClick(getFinder().find(new NameMatcher("emailRadiobutton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("contactInformationTextfield")), "nibbler@futurama.com");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("okButton")));
	    
	    TableModel content = ((JTable) getFinder().find(new NameMatcher("viewTable"))).getModel();
	    assertEquals("Tääst is not the first", "Tääst", content.getValueAt(0, 1));
	    assertEquals("Täest is not the second", "Täest", content.getValueAt(1, 1));
	}
	
	@Test
	public void testOrderingWithSZ() throws ComponentNotFoundException, MultipleComponentsFoundException{
	    // Add new users
	    buttonTester.actionClick(getFinder().find(new NameMatcher("addButton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("firstNameTextfield")), "Teßt");
	    textTester.actionEnterText(getFinder().find(new NameMatcher("lastNameTextfield")), "Dr Zoidberg");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("maleRadiobutton")));
	    buttonTester.actionClick(getFinder().find(new NameMatcher("okButton")));
	    
	    buttonTester.actionClick(getFinder().find(new NameMatcher("addButton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("firstNameTextfield")), "Teszt");
	    textTester.actionEnterText(getFinder().find(new NameMatcher("lastNameTextfield")), "Dr Zoidberg");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("maleRadiobutton")));
	    buttonTester.actionClick(getFinder().find(new NameMatcher("emailRadiobutton")));
	    textTester.actionEnterText(getFinder().find(new NameMatcher("contactInformationTextfield")), "zoidberg@futurama.com");
	    buttonTester.actionClick(getFinder().find(new NameMatcher("okButton")));
	    
	    TableModel content = ((JTable) getFinder().find(new NameMatcher("viewTable"))).getModel();
	    assertEquals("Teßt is not the first", "Teßt", content.getValueAt(0, 0));
	    assertEquals("Teszt is not the second", "Teszt", content.getValueAt(1, 0));
	}
	
}