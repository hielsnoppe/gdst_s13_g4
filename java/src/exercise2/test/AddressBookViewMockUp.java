/*
 * Example code used in exercises for lecture "Grundlagen des Software-Testens"
 * Created and given by Ina Schieferdecker and Edzard Hoefig
 * Freie Universitaet Berlin, SS 2013
 */
package exercise2.test;

import exercise2.addressbook.controller.AddressBookController;
import exercise2.addressbook.model.AddressBookAccess;
import exercise2.addressbook.view.AddressBookView;

/**
 * Uebung 2 - Komponenten und Integrationstest
 * Mock-Up für den AddressBookView
 * 
 * Bitte Nummer der Gruppe eintragen:
 * 4
 * 
 * Bitte Gruppenmitglieder eintragen:
 * @author Hoppe, Niels
 * @author Junker, Severin
 * @author Malotky, Troels
 * @author Neumann, Daniel
 */
public class AddressBookViewMockUp implements AddressBookView {

	@Override
	public void create(AddressBookAccess model, AddressBookController controller) {
		// do nothing
		
	}

	@Override
	public void refresh() {
		// do nothing
		
	}
	
}
