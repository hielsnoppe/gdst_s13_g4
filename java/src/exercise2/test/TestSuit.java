package exercise2.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Uebung 2 - TestSuite
 * TestSuite für die Tests
 * 
 * Bitte Nummer der Gruppe eintragen:
 * 4
 * 
 * Bitte Gruppenmitglieder eintragen:
 * @author Hoppe, Niels
 * @author Junker, Severin
 * @author Graf von Malotky, Troels Nikolaj
 * @author Neumann, Daniel
 */

@RunWith(Suite.class)
@SuiteClasses( {AddressBookControllerTest.class, ControllerAddressBookIntegrationTest.class} )
public class TestSuit {

	// nothing todo here
	
}
