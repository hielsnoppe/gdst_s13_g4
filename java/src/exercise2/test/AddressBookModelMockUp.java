/*
 * Example code used in exercises for lecture "Grundlagen des Software-Testens"
 * Created and given by Ina Schieferdecker and Edzard Hoefig
 * Freie Universitaet Berlin, SS 2013
 */
package exercise2.test;

import java.io.IOException;
import java.util.Collection;

import exercise2.addressbook.model.AddressBookModel;
import exercise2.addressbook.model.Entry;
import exercise2.addressbook.model.SizeLimitReachedException;

/**
 * Uebung 2 - Komponenten und Integrationstest
 * Mock-Up für das AddressBookModel
 * 
 * Bitte Nummer der Gruppe eintragen:
 * 4
 * 
 * Bitte Gruppenmitglieder eintragen:
 * @author Hoppe, Niels
 * @author Junker, Severin
 * @author Malotky, Troels
 * @author Neumann, Daniel
 */
public class AddressBookModelMockUp implements AddressBookModel {

	private int entryCount = 0;
	public final static int sizeLimit = 10;
	
	@Override
	public boolean addEntry(Entry entry) throws SizeLimitReachedException {
		// Check size limit
		if (entryCount >= sizeLimit) 
			throw new SizeLimitReachedException("Limit is " + sizeLimit);
		
		// Increase number of entries
		++entryCount;
		
		return true;
	}

	@Override
	public Entry getEntry(String surName, String firstName) {
		// do nothing
		return null;
	}

	@Override
	public Collection<Entry> getEntries() {
		// do nothing
		return null;
	}

	@Override
	public boolean deleteEntry(Entry entry) {
		// do nothing
		return false;
	}

	@Override
	public void erase() {
		// do nothing
	}

	@Override
	public void load() throws IOException {
		// do nothing
	}

	@Override
	public void save() throws IOException {
		// do nothing
	}
}
