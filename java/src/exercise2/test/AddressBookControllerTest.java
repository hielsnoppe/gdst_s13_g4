/*
 * Example code used in exercises for lecture "Grundlagen des Software-Testens"
 * Created and given by Ina Schieferdecker and Edzard Hoefig
 * Freie Universitaet Berlin, SS 2013
 */
package exercise2.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import exercise2.addressbook.controller.AddressBookController;
import exercise2.addressbook.controller.AddressBookControllerImpl;
import exercise2.addressbook.controller.ParameterException;
import exercise2.addressbook.model.SizeLimitReachedException;


/**
 * Uebung 2 - Komponenten und Integrationstest
 * Komponententests für den Controller
 * 
 * Bitte Nummer der Gruppe eintragen:
 * 4
 * 
 * Bitte Gruppenmitglieder eintragen:
 * @author Hoppe, Niels
 * @author Junker, Severin
 * @author Graf von Malotky, Troels Nikolaj
 * @author Neumann, Daniel
 */
public class AddressBookControllerTest {
	
	/*
	 *  Aufgabe 3
	 *  Führen Sie im Rahmen eines Komponententests der Klasse exercise2.addressbook.controller.AddressBookControllerImpl einen Test der Methode add(...) durch.
	 *  Schreiben Sie für die model und view Komponenten Mock-Up Klassen und verwenden Sie dies im Komponententest der AddressBookController Komponente.
	 *  Testen Sie die add() Methode auf Herz und Nieren - es sind durchaus Fehler zu finden.
	 */
	
	// Model component for the test
	AddressBookModelMockUp model;
	
	// View component for the test
	AddressBookViewMockUp view;
	
	// Controller component for the test
	AddressBookController controller;
	
	/**
	 * Set up test system
	 */
	@Before
	public void setUp() {
		// Instantiate and wire components
		this.model = new AddressBookModelMockUp();
		this.view = new AddressBookViewMockUp();
		this.controller = new AddressBookControllerImpl(model, view);
	}
	
	// --------------------------------------
	// Testfälle für den Komponententest
	@Test
	public void testAddCorrectUsers(){
		// add first user
		try {
			controller.add("Stan", "Marsh", "M", "12345", null);
		} catch (ParameterException e) {
			fail("Unexpected Exception");
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
		
		// add second user
		try {
			controller.add("Sharon", "Marsh", "F", null, "sharon@southphark.com");
		} catch (ParameterException e) {
			fail("Unexpected Exception");
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
		
		// add third user
		try {
			controller.add("Eric", "Cartman", "M", null, null);
		} catch (ParameterException e) {
			fail("Unexpected Exception");
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
		

		// add other users till size limit reached
		for(int i=3; i < AddressBookModelMockUp.sizeLimit + 1; ++i){
			try {		
				controller.add("Dude "+i, "Duke", "M", null, null);
				assertTrue("sizeLimit not correctly reached: actual size(before add) = "+i+", sizeLimit = "+AddressBookModelMockUp.sizeLimit, i < AddressBookModelMockUp.sizeLimit);
			} catch (ParameterException e) {
				fail("Unexpected Exception");
			} catch (SizeLimitReachedException e) {
				assertEquals("sizeLimit not correctly reached", AddressBookModelMockUp.sizeLimit, i);
			}
		}
	}
	
	@Test
	public void testAddWrongNames() {
		// add user with wrong parameters
		try {
			controller.add(null, "Marsh", "F", "12345", null);
			fail("User has a null first name");
		} catch (ParameterException e) {
			// expected behavior
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
		
		// add user with wrong parameters
		try {
			controller.add("Sharon", null, "F", "12345", null);
			fail("User has a null last name");
		} catch (ParameterException e) {
			// expected behavior
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
	}
	
	@Test
	public void testAddWrongGenderLowercase() {
		// add user with wrong parameters
		try {
			controller.add("Randy", "Marsh", "X", null, "randy@southphark.com");
			fail("User has no correct gender");
		} catch (ParameterException e) {
			// expected behavior
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
		
		// add user with wrong parameters
		try {
			controller.add("Randy", "Marsh", "FM", null, "randy@southphark.com");
			fail("User has no correct gender");
		} catch (ParameterException e) {
			// expected behavior
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
		
		// add user with wrong parameters
		try {
			controller.add("Randy", "Marsh", "m", null, "randy@southphark.com");
			fail("User has no correct gender: lowercase letter");
		} catch (ParameterException e) {
			// expected behavior
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
	}
	
	@Test
	public void testAddWrongGenderNull() {
		// add user with wrong parameters
		try {
			controller.add("Randy", "Marsh", null, null, "randy@southphark.com");
			fail("User has no correct gender");
		} catch (ParameterException e) {
			/// expected behavior
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		} 
	}
	
	@Test
	public void testAddBothEmailAndPhoneNumber() {
		// add user with wrong parameters
		try {
			controller.add("Randy", "Marsh", "M", "12345", "randy@southphark.com");
			fail("User has both email and phone number");
		} catch (ParameterException e) {
			// expected behavior
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		} catch (NullPointerException e) {
			fail("User has a null gender. NullPointerException instead of ParameterException");
		}
	}
	
	@Test
	public void testAddNegativePhoneNumber() {
		// add user with wrong parameters
		try {
			controller.add("Randy", "Marsh", "M", "-1", null);
			fail("User has a negative phone number");
		} catch (ParameterException e) {
			// expected behavior
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
	}
	
	@Test
	public void testAddNonDigitPhoneNumber() {
		// add user with wrong parameters
		try {
			controller.add("Randy", "Marsh", "M", "1.0a", null);
			fail("User has a non digit in phone number");
		} catch (ParameterException e) {
			// expected behavior
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		} catch (NumberFormatException e) {
			fail("Non digit Phone Number. NumberFormatException instead of ParameterException");
		}
	}
	
}