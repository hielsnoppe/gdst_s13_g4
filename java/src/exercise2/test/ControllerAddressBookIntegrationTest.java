/*
 * Example code used in exercises for lecture "Grundlagen des Software-Testens"
 * Created and given by Ina Schieferdecker and Edzard Hoefig
 * Freie Universitaet Berlin, SS 2013
 */
package exercise2.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import exercise2.addressbook.controller.AddressBookController;
import exercise2.addressbook.controller.AddressBookControllerImpl;
import exercise2.addressbook.controller.ParameterException;
import exercise2.addressbook.model.AddressBookModel;
import exercise2.addressbook.model.AddressBookModelImpl;
import exercise2.addressbook.model.Entry;
import exercise2.addressbook.model.SizeLimitReachedException;

/**
 * Uebung 2 - Komponenten und Integrationstest
 * Integration Test für Addressbook und Controller.
 * 
 * Bitte Nummer der Gruppe eintragen:
 * 4
 * 
 * Bitte Gruppenmitglieder eintragen:
 * @author Hoppe, Niels
 * @author Junker, Severin
 * @author Graf von Malotky, Troels Nikolaj
 * @author Neumann, Daniel
 */
public class ControllerAddressBookIntegrationTest {

	// Location of the address book file
	private static final File addressBookFile = new File("contacts.xml");
		
	/*
	 *  Aufgabe 4
	 *  Programmieren Sie einen Integrationstest für AddressBookModel und AddressBookController.
	 *  Testen Sie ob die Methoden des exercise2.addressbook.controller.AddressBookController Interface zu den erwarteten Resultate im Addressbuch führen.
	 *  Testen Sie intensiv und schreiben Sie MINDESTENS einen Testfall pro Methode des interfaces. Es sind Fehler zu finden.  
	 */
	
	// Model component for the test
	AddressBookModel model;
	
	// View component for the test
	AddressBookViewMockUp view;
	
	// Controller component for the test
	AddressBookController controller;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		// Instantiate and wire components
		this.model = new AddressBookModelImpl(addressBookFile);
		this.view = new AddressBookViewMockUp();
		this.controller = new AddressBookControllerImpl(model, view);
	}
	
	/*=========================================================================
	 * 
	 * TESTING ADD
	 * 
	 *=======================================================================*/
	
	// firstname not null
	// lastname not null
	// gender has to be M or F
	// phone, only positiv numbers and only digits, if email this has to be null
	// email, if phone, this has to be null
	// parameter exception, thrown when parameter is incorrect, no entry to address book
	// sizelimit, when full (max. 10), no entry to addressbook
	// no outut
	
	@Test
	public void testAddWithCorrectParameters(){

		// !!
		// warum sind die eingaben eigentlich verdreht zwischen controller und model
		// cotroller, add( firstname, lastname )
		// model, getEntry( surname, firstname )
		// => brainfuck
		
		try {
			// insert new guy
			this.controller.add("Stan", "Marsh", "M", "12345", null);

			// check this
			Entry response = this.model.getEntry("Marsh", "Stan");
			assertNotNull("That's stupid", response );
			
		} catch (ParameterException e) {
			fail("Parameter should be correct");
		} catch (SizeLimitReachedException e) {
			fail("addressbook should not be full");
		}

		
		try {
			this.controller.add("Sharon", "Marsh", "F", null, "sharon@southphark.com");
			
			// check this
			Entry response = this.model.getEntry("Marsh", "Sharon");
			assertNotNull("That's stupid", response );
			
		} catch (ParameterException e) {
			fail("Parameter should be correct");
		} catch (SizeLimitReachedException e) {
			fail("addressbook should not be full");
		}
		
	}
	
	@Test
	public void testAddWithIncorrectSurname(){
		
		assertEquals("There should no entry", this.model.getEntries().size(), 0);
		
		// add user with wrong parameters
		try {
			this.controller.add("Sharon", null, "F", "12345", null);
		} catch (ParameterException e) {
			Entry response = this.model.getEntry(null, "Sharon");
			assertNull("Sharon should not exists", response);
			assertEquals("There should no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
		
	}

	@Test
	public void testAddWithIncorrectFirstname(){
		
		assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
		
		// add user with wrong parameters
		try {
			this.controller.add(null, "Marsh", "F", "12345", null);
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("Marsh", null);
			assertNull("Sharon should not exists", response);
			assertEquals("There should no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
		
	}
	
	@Test
	public void testAddWithIncorrectGenderNull(){
		
		assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
		
		try {
			this.controller.add("Eric", "Cartman", null, null, "eric.cartman@southpark.com");
			fail("You added Cartman");
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("Cartman", "Eric");
			assertNull("Eric should not exists", response);
			assertEquals("There should no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		} catch (NullPointerException e){
			fail("NULL is not handled");
		}
	}
	
	@Test
	public void testAddWithIncorrectGenderChar(){
		try {
			this.controller.add("Eric", "Cartman", "X", null, "eric.cartman@southpark.com");
			fail("You added Cartman");
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("Cartman", "Eric");
			assertNull("Eric should not exists", response);
			assertEquals("There should no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}

		
		try {
			this.controller.add("Kyle", "Broflovski", "m", null, "kyle@southpark.com");
			fail("You added Kyle");
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("Broflovski", "Kyle");
			assertNull("Kyle should not exists", response);
			assertEquals("There should no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
	
	}
	
	@Test
	public void testAddWithIncorrectPhoneNull(){
		
		assertEquals("There should no entry", model.getEntries().size(), 0);
		
		try {
			this.controller.add("Kenny", "McCormick", "M", null, null);
			fail("This should never happen");
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("McCormick", "Kenny");
			assertNull("Kenny should not exists", response);
			assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
	}
	
	@Test
	public void testAddWithIncorrectPhoneMinus(){

		assertEquals("There should no entry", this.model.getEntries().size(), 0);
		
		try {
			this.controller.add("Kenny", "McCormick", "M", "-123", null);
			fail("You create a Kenny :)");
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("McCormick", "Kenny");
			assertNull("Kenny should not exists", response);
			assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
	}
	
	@Test
	public void testAddWithIncorrectPhoneMinusAlpha(){

		assertEquals("There should no entry", this.model.getEntries().size(), 0);
		
		try {
			this.controller.add("Kenny", "McCormick", "M", "-123a", null);
			fail("This should never happen");
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("McCormick", "Kenny");
			assertNull("Kenny should not exists", response);
			assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		} catch (NumberFormatException e){
			Entry response = this.model.getEntry("McCormick", "Kenny");
			assertNull("Kenny should not exists", response);
			assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
			fail("Number format not checked");
		}

	}
	
	@Test 
	public void testAddWithIncorrectPhoneHex(){

		assertEquals("There should no entry", this.model.getEntries().size(), 0);
		
		try {
			this.controller.add("Kenny", "McCormick", "M", "0x15", null);
			fail("You create a kenny");
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("McCormick", "Kenny");
			assertNull("Kenny should not exists", response);
			assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		} catch (NumberFormatException e){
			Entry response = this.model.getEntry("McCormick", "Kenny");
			assertNull("Kenny should not exists", response);
			assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
			fail("Number format failed");
		}
		
	}
	
	@Test
	public void testAddWithIncorrectPhoneInteressting(){

		assertEquals("There should no entry", this.model.getEntries().size(), 0);
		
		// When you remove the method "fail('this should never happen')
		// you can see that the first kenny will be created
		
		// 1. Test
		try {
			this.controller.add("Kenny", "McCormick", "M", "-123", null);
			fail("This should never happen (1)");
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("McCormick", "Kenny");
			assertNull("Kenny should not exists (1)", response);
			assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
		
		// 2. Test
		try {
			this.controller.add("Kenny", "McCormick", "M", "0x15", null);
			fail("This should never happen (2)");
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("McCormick", "Kenny");
			assertNull("Kenny should not exists (2)", response);
			assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		} catch (NumberFormatException e){
			Entry response = model.getEntry("McCormick", "Kenny");
			assertNull("Kenny should not exists (3)", response);
			assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
			fail("Number format failed");
		}
		
	}
	
	@Test
	public void testAddWithIncorrectPhoneMail(){

		assertEquals("There should no entry", this.model.getEntries().size(), 0);
		
		try {
			this.controller.add("Kenny", "McCormick", "M", "123", "kenny@southpark.com");
			fail("This should never happen");
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("McCormick", "Kenny");
			assertNull("Kenny should not exists", response);
			assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}

		try {
			this.controller.add("Kenny", "McCormick", "M", "-123", "kenny@southpark.com");
			fail("You create a kenny");
		} catch (ParameterException e) {
			Entry response = this.model.getEntry("McCormick", "Kenny");
			assertNull("Kenny should not exists", response);
			assertEquals("There shoudl no entry", this.model.getEntries().size(), 0);
		} catch (SizeLimitReachedException e) {
			fail("Unexpected Exception");
		}
		
		
	}
	
	@Test(expected = SizeLimitReachedException.class)
	public void testAddWithIncorrectSizeLimit() throws ParameterException, SizeLimitReachedException{
	
		// fill the addressbook
		for(int i=0; i < AddressBookModelImpl.sizeLimit; ++i){
			try {
				this.controller.add("Dude "+i, "Duke", "M", "123", null);
				
			} catch (ParameterException e) {
				fail("Unexpected Exception");
			} catch (SizeLimitReachedException e) {
				fail("Unexpected Exception");
			}
		}
		
		// add one more
		this.controller.add("Fass", "zum Ueberlaufen", "M", "123", null);
		
	}
	
	/*=========================================================================
	 * 
	 * TESTING REMOVE
	 * 
	 *=======================================================================*/
	
	// remove of an existing entry
	// entry_no = starts with 0
	// entries are alphabetic ordered
	// entry_no is not allowed to refer to a non existing entry
	// entry_no should smaller than the entries in the address book
	// after deleting, the addressbook will not contain the referenced entry
	// no output
	// no exceptions
	
	@Test
	public void testRemoveAnExistingEntry(){
		
		// add test entry
		try {
			this.controller.add("Randy", "Marsh", "M", null, "randy@southphark.com");
		} catch (ParameterException e) {
			fail("Parameters should be OK");
		} catch (SizeLimitReachedException e) {
			// TODO Auto-generated catch block
			fail("size should be OK");
		}
		
		int start_size = this.model.getEntries().size();
		
		// try to delete the first entry
		this.controller.remove(0);
		
		int end_size = this.model.getEntries().size();
		
		assertEquals("The size of the address book should be reduced by one", end_size, (start_size-1));
		
	}
	
	@Test
	public void testRemoveCheckOrdering(){
		
		// create test scenario
		try {
			this.controller.add("C","C","M","789",null);
			this.controller.add("B","B","F","456",null);
			this.controller.add("A","A","M","123",null);
		} catch (ParameterException e) {
			fail("Parameter fail");
		} catch (SizeLimitReachedException e) {
			fail("sizeLimit fail");
		}
		
		assertEquals("Size should be three", this.model.getEntries().size(), 3);
		
		// delete B -> entry_no = 1
		this.controller.remove(1);
		assertEquals("Size should be two", this.model.getEntries().size(), 2);
		
		// test if really B is deleted
		assertNull("B should be waste", this.model.getEntry("B", "B"));
		assertNotNull("A should be alive", this.model.getEntry("A", "A"));
		assertNotNull("C should be alive", this.model.getEntry("C", "C"));

		// delete A -> entry_no = 0
		this.controller.remove(0);
		assertEquals("Size should be one", this.model.getEntries().size(), 1);
		
		// test if really A is deleted
		assertNull("A should be waste", this.model.getEntry("A", "A"));
		assertNotNull("C should be alive", this.model.getEntry("C", "C"));

		// delete C -> now it should be entry_no = 0
		this.controller.remove(0);
		assertEquals("Size should be zero", this.model.getEntries().size(), 0);

		// test if C is really deleted
		assertNull("A should be waste", this.model.getEntry("C", "C"));
		
	}
	
	@Test
	public void testRemoveReferLowerNumber(){
	
		assertEquals("Size should be zero", this.model.getEntries().size(), 0);
		
		try{
			// try to delete an entry_no lower than 0
			this.controller.remove(-1);
			
		}catch(ArrayIndexOutOfBoundsException e){
			fail("The boundaries aren't checked. Bad Guy");
		}		
	}
	
	@Test
	public void testRemoveReferNotExists(){
		
		assertEquals("Size should be zero", this.model.getEntries().size(), 0);
		
		try{
			// try to delete non existing entry
			this.controller.remove(0);
			
		}catch(ArrayIndexOutOfBoundsException e){
			fail("The boundaries aren't checked. Bad Guy");
		}		
			
	}
	
	@Test
	public void testRemoveEntryNoShouldSmallerThanEntries(){

		try {
			this.model.load();
		} catch (IOException e) {
			fail("IO");
		} 
		
		assertTrue("There should be data", this.model.getEntries().size() > 0 );
		
		this.controller.remove( this.model.getEntries().size() - 1);
	}
	
	/*=========================================================================
	 * 
	 * TESTING ERASE
	 * 
	 *=======================================================================*/

	// removes all entry 
	// the addressbook should be empty
	// no input
	// no output
	// no exception
	
	@Test
	public void testEraseEmptyBook(){
		
		// delete everything and check the size of the addressbook
		
		this.model.erase();
		
		int size = this.model.getEntries().size();
		
		assertEquals("Size of entries should be zero", size, 0);
	}

	@Test
	public void testEraseLoadedBook(){
		
		try {
			this.model.load();
			
			assertTrue("Entries should more than one", ( this.model.getEntries().size()>0 ));
			
			this.model.erase();
			
			assertTrue("Size should be zero", ( 0 == this.model.getEntries().size()));
			
		} catch (IOException e) {
			fail("IO");
		}
	}
}
