<!--- <link href="http://kevinburke.bitbucket.org/markdowncss/markdown.css" rel="stylesheet"></link> -->

# Übung 5

## Iterator State Machine

![NO IMAGE STATE MACHINE](aufgabe5/state_machine/iterator.states.png)


## Iterator Baumdiagramm

![NO IMAGE ITERATOR TREE](aufgabe5/iterator_tree/iterator.tree.png)


## Testfälle
``
 <init> Constructor <begin> Destructor <final>
 <init> Constructor <begin> itemCount == 0 <end> Destructor <final>
 <init> Constructor <begin> itemCount == 0 <end> first <begin>
 <init> Constructor <begin> next <between> Destructor <final>
 <init> Constructor <begin> next <between> first <begin>
 <init> Constructor <begin> next <between> next, itemCount == max <end>
 <init> Constructor <begin> next <between> next, itemCount < max <between>
``
