# Übung 4

## Aufgabe 1
Programmcode:

```java
1.	public static int suche(final char zeichen, final char[] alphabet) {
2.		int erstes = 0; 
3.		int letztes = alphabet.length - 1;
4.		
5.  	while (erstes <= letztes) {
6.  		final int mitte = erstes + ((letztes - erstes) / 2);
7.			if (alphabet[mitte] < zeichen) {
8.				erstes = mitte + 1;		// Rechts weitersuchen
9.			} else if (alphabet[mitte] > zeichen) {
10.				letztes = mitte - 1;	// Links weitersuchen
11.			} else {
12.		  		return mitte;			// Zeichen gefunden
13.			}
14.  	}
15.  	return -1;						// Zeichen nicht gefunden
16.	}
```

### Teilaufgabe a
Anweisungen = 2., 3., 6., 8., 10., 12., 15.
Gesamtzahl Anweisungen = 7

Wir konstruieren zwei Testfälle.
Der eine (T1) dient zum Erreichen des `return` außerhalb der Schleife,
der andere (T2) zum Erreichen des `return` innerhalb der Schleife.
Es stellt sich heraus, dass damit eine vollständige Anweisungsüberdeckung gegeben ist.

```java
// T1
char zeichen = 'a';
char[] alphabet = {};
suche(zeichen, alphabet);
```
Pfad(T1) = (1,2-4,5,14-15,16)
Erreichte Anweisungen = 2., 3., 15.

Für T2 berechnen wir geeignete Werte für die Länge von `alphabet.length` und die Position `pos` des gesuchten Zeichens darin.
Dafür gehen wir den gewünschten Programmablauf durch und notieren jeweils die benötigte Belegung der Variablen.

```java
length = alphabet.length    // added for readability
erstes = 0
letztes = length - 1
mitte = 0 + ((length - 1) / 2)
      = (length - 1) / 2
      < pos                 // enable first if
erstes = (length - 1) / 2 + 1
mitte = (length - 1) / 2 + 1 + (((length - 1) - ((length - 1) / 2 + 1)) / 2)
      > pos                 // enable second if
letztes = (length - 1) / 2 + 1 + (((length - 1) - ((length - 1) / 2 + 1)) / 2) - 1
mitte = (length - 1) / 2 + 1 + ((((length - 1) / 2 + 1 + (((length - 1) - ((length - 1) / 2 + 1)) / 2) - 1) - ((length - 1) / 2 + 1)) / 2)
      = pos                  // enable last else
```

In die zuletzt erhaltene Beziehung zwischen `pos` und `length` setzen wir testweise `length = 6` ein und erhalten dadurch:

```java
pos = 3 + (((3 + ((5 - 3) / 2) - 1) - 3) / 2)
    = 3 + (((3 + 0 - 3) / 2)
    = 3
```

Es ergibt sich also als konkreter Testfall:

```java
// T2
char zeichen = 'd';
char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f'};
suche(zeichen, alphabet);
```
Pfad(T2) = (1,2-4,5,6-7,8,5,6-7,9,10,5,6-7,9,11-13,16)
Erreichte Anweisungen = 2., 3., 6., 8., 10., 12.

Anzahl durchlaufener Anweisungen insgesamt = 7
Anweisungsüberdeckunggrad = (Anzahl durchlaufener Anweisungen insgesamt / Gesamtzahl Anweisungen) * 100 % = (7 / 7) * 100 % = 100 %


### Teilaufgabe b
Die Testfälle aus Teilaufgabe a erreichen auch eine vollständige Zweigüberdeckung.

Es werden alle Zweige des Kontrollflussgraphen durch die Pfade der beiden Testfälle T1 und T2 abgedeckt.


### Teilaufgabe c


#### Datenflußbasierte Tests

def( zeichen, alphabet )
def( erstes )
def( letztes, c-use ( alphabet )
p-use(erstes, letztes )
def(mitte), c-use(erstes,letztes)
p-use(alphabet, mitte, zeichen )
def(erstes), c-use(mitte)
p-use (alphabet, mitte, zeichen )
def(letztes), c-use(mitte)
c-use(mitte)

#### DU-Kette

* zeichen

   [zeichen, /0/, if (alphabet[mitte] < zeichen ]
   [zeichen, /0/, if (alphabet[mitte] > zeichen ]

* alphabet

   [alphabet, /0/, int letztes = alphabet.length -1 ]
   [alphabet, /0/, if(alphabet[mitte] < zeichen ]
   [alphabet, /0/, if(alphabet[mitte] > zeichen ]

* erstes

   [erstes, erstes=0, while( erstes <= letztes) ]
   [erstes, erstes=0, final int mitte = erstes + ((letztes - erstes) / 2) ]
   [erstes, erstes=mitte+1, final int mitte = erstes + ((letztes - erstes) / 2) ]
   [erstes, erstes=mitte+1, while(erstes <= letztes) ]
   
* letztes

   [letztes, letztes = alphabet.length ­ 1, while (erstes <= letztes)]
   [letztes, letztes = alphabet.length ­ 1, final int mitte = erstes + ((letztes - erstes) / 2)]
   [letztes, letztes = mitte - 1, while (erstes <= letztes)]
   [letztes, letztes = mitte - 1, final int mitte = erstes + ((letztes - erstes) / 2)]

* mitte

   [mitte, final int mitte = erstes + (( letztes-erstes) /2 ), if (alphabet[mitte] < zeichen) ]
   [mitte, final int mitte = erstes + (( letztes-erstes) /2 ), erstes = mitte + 1 ]
   [mitte, final int mitte = erstes + (( letztes-erstes) /2 ), if (alphabet[mitte] > zeichen) ]
   [mitte, final int mitte = erstes + (( letztes-erstes) /2 ), erstes = mitte - 1 ]
   [mitte, final int mitte = erstes + (( letztes-erstes) /2 ), return mitte ]

### Teilaufgabe d
Die Testfälle aus Teilaufgabe a erreichen decken ebenfalls alle DU Ketten ab, da sowohl eine vollständige Zweig- als auch Anweisungsüberdeckung vorliegt.


## Aufgabe 2

### Teilaufgabe a

```
a := Alter <= 18
g := Geschlecht = maennlich
t := Telefonnummer vorhanden
F := a UND g UND t
```

```
a g t F
-------
0 0 0 0
0 0 1 0
0 1 0 0
0 1 1 0
1 0 0 0
1 0 1 0
1 1 0 0
1 1 1 1
```

### Teilaufgabe b

```
a := Alter > 30
g := Geschlecht = weiblich
n1 := Nachname = "Duck"
n2 := Nachname = "Maus"
F := g UND (n1 ODER n2) UND a
```

```
a g n1 n2 F
-----------
0 0 0  0  0
0 0 0  1  0
0 0 1  0  0
0 0 1  1  0
0 1 0  0  0
0 1 0  1  0
0 1 1  0  0
0 1 1  1  0
1 0 0  0  0
1 0 0  1  0
1 0 1  0  0
1 0 1  1  1
1 1 0  0  0
1 1 0  1  1
1 1 1  0  0
1 1 1  1  1
```

### Teilaufgabe c

```
a1 := Alter < 18
a2 := Alter > 65
g1 := Geschlecht = weiblich
g2 := Geschlecht = maennlich
n := Nachname beginnt mit "D"
F := n UND (g1 ODER (g2 UND (a1 ODER a2)))

```

```
a1 a2 g1 g2 n F
---------------
0  0  0  0  0 0
0  0  0  0  1 0
0  0  0  1  0 0
0  0  0  1  1 0
0  0  1  0  0 0
0  0  1  0  1 0
0  0  1  1  0 0 
0  0  1  1  1 0
0  1  0  0  0 0
0  1  0  0  1 0
0  1  0  1  0 0
0  1  0  1  1 0
0  1  1  0  0 0
0  1  1  0  1 0
0  1  1  1  0 0
0  1  1  1  1 0
1  0  0  0  0 0
1  0  0  0  1 0
1  0  0  1  0 0
1  0  0  1  1 0
1  0  1  0  0 0
1  0  1  0  1 1
1  0  1  1  0 1
1  0  1  1  1 1
1  1  0  0  0 1
1  1  0  0  1 1
1  1  0  1  0 1
1  1  0  1  1 1
1  1  1  0  0 1
1  1  1  0  1 1
1  1  1  1  0 1
1  1  1  1  1 1
```

### Teilaufgabe d

Eine Bedingung wird bei "Lazy Evaluation" nur solange geprüft bis der 
Wahrheitswert ermittelt ist. Demnach würden einige atomare Ausdrücke
vermutlich nicht geprüft werden. Konkret bezogen auf die Testfälle
von 2a bis 2c, wären das alle Tests bei denen (a oder a1) den Wert
FALSE besitzen.
