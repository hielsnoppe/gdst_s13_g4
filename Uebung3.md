# Übung 3

## Aufgabe 1

### Berechnen der zyklomatischen Zahl

   e = Kanten     = 11
   v = Knoten     = 9
   k = Konstante  = 2

   cv = e  - v + k
      = 11 - 9 + 2
      = 4


### Datenflußanalyse

   Vor- und Nachfolger Tabelle
   ===========================


   -------------------------------------
   Anweisung   | Vorgänger | Nachfolger
   -------------------------------------
      6           -           7
      7           6           8
      8           7           9,18
      9           8           10
      10          9           11,13
      11          10          8
      13          10          14,16
      14          13          8
      16          13          8
      18          8           19
      19          18          -


   Datenflußanomalie
   =================

1.  public static final int NOTFOUND = -1;                        // d(NOTFOUND)
2.  // Binaere Suche auf Array a.
3.  // Annahme: a ist sortiert
4.  // Ergebnis: NOTFOUND, wenn k nicht in A enthalten ist.
5.  // Ergebnis: i falls a[i] gleich k ist.
6.  public static int binSearch(int[] a, int k) {                 // d(a,k)
7.    int ug = 0, og = a.length-1, m = 0, pos = NOTFOUND;         // d(ug, og, m, pos), r(a,NOTFOUND)
8.    while (ug <= og && pos == NOTFOUND) {                       // r(ug, og, pos, NOTFOUND)
9.      m = (ug + og) / 2;                                        // d(m), r(ug, og)
10.     if (a[m] == k)                                            // r(a,k)
11.       pos = m;                                                // d(pos),r(m)
12.     else
13.       if (a[m] < k)                                           // r(a,k)
14.          ug = m + 1;                                          // d(ug), r(m)
15.       else
16.          og = m - 1;                                          // d(og), r(m)
17.   }
18.   return pos;                                                 // r(pos)
19. }

## Aufgabe 2

Testing Guidelines
==================

   1) Keep testing at unit level
   =============================

      Für die bisherigen Komponententest haben wir uns an dieses Schema
      gehalten. Für die Integrationstest, war es allerdings unabdingbar
      mehrere Klassen mittels einer Testklasse zu prüfen.


   2) Name test properly
   =====================

      Unsere Testklasse zeigt, dass wir uns, wenn vielleicht auch nicht bewußt,
      an das Schemata weitestgehend gehalten haben. Hierzu ein paar
      Beispiele aus unserer Testklasse :

      * testAddWithIncorrectSurname()
      * testRemoveAnExistingEntry()
      * testEraseLoadedBook()


   3) Test the trivial cases, too
   ==============================

      In dem Sinne haben wir bisher keine trivialen Klassen getestet.
      Der Testumfang richtete sich bei unseren stets nach der geforderten
      Aufgabenstellung.
      

   4) Focus on execution coverage first
   ====================================

      Die Parameter einer Funktion sind hierbei ein entscheidender Faktor,
      ob diese Methode wirklich hunderprozentig geprüft werden kann.
      
      Bei Parameter die z.B. Zahlen verlangen, kann das zu testende Spektrum
      sehr groß werden. In diesem Zusammenhang versucht man dann auch
      eher mit typischen Eingaben oder Grenzfällen zu arbeiten.
   

   5) Cover boundary cases
   =======================

      Die Abdeckungen von Grenzfällen haben wir soweit wie möglich
      versucht Umzusetzen.


   6) Provide a random generator
   =============================


      Bisher wurde kein Zufallsgenerator eingesetzt.


   7) Test each feature once
   =========================

      Haben wir so gut es geht versucht umzusetzen.


   8) Use explicit asserts
   =======================

      OK


   9) Provide negative tests
   =========================

      Haben wir auch so umgesetzt, eine weitere Vorgehensweise die 
      wir für solche Fälle benutzt haben, ist es, die zu erwartende
      Exception (Ausnahme) mit im "Testkopf" zu deklarieren.

      @Test(expected=SizeLimitReached.class)      


   10) Prepare test code for failures
   ==================================

      Einige stellen in unserem Code weisen zwar auch diese Problematik
      auf. Dennoch haben wir versucht, durch auslagern bestimmter Codeteile in 
      andere Testprozeduren der Guideline gerecht zu werden.

