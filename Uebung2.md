# Übung 2

## Aufgabe 1

> Nennen Sie sechs Faktoren, die die Qualitätssicherung heutiger software-basierter Systeme anspruchsvoll machen.

Die Qualität eines Programms in der heutigen Zeit ist aufwändiger aufrecht zu
erhalten als früher, seit circa 1980 gibt es eine sogenannte „Softwarekrise”, welche
die Kosten für hohe Qualität immer weiter erhöht, dafür gibt es mehrere Gründe,
einige sind hier genannt.

1. Programme sind im Verlauf der Zeit viel komplexer geworden. Das liegt vor
allem an dem Umfang eines Programms. Desto mehr Zeilen Code ein
Programm ein besitzt, umso höher ist die Fehleranfälligkeit. Die
Entwicklungskosten sind so immens gestiegen, dass es schon ohne
Qualitätssicherung ein hohes Budget verlangt wird.

2. Die Benutzer haben einen hohen Anspruch an die GUI. Die grafische
Oberfläche ist viel komplexer geworden, aber auch die Benutzbarkeit muss an
die Zielgruppe angepasst sein. Denn es arbeiten mehr und mehr Menschen mit
Programmen, welche über kein technisches Wissen verfügt. Der Komfort für die
Nutzer erhört weiterhin die Komplexität

3. Durch das immer weiter verbreitete Internet sind fast alle Endgeräte global
vernetzt. Dies stellt ein weiteres Sicherheitsrisiko dar. Es hat sich auch eine
professionelle Kriminalität entwickelt, welche sich auf digitalen Datendiebstahl
spezialisiert hat. Die Qualität wird auch durch Sicherheit des Systems definiert.
Kleinere Sicherheitslücken werden heutzutage weitaus stärker ausgenutzt als
früher. Durch die Globalisierung sind verkaufte Programme auch viel schneller
weltweit verkauft. Dadurch muss auch viel mehr Integration in den spezifischen
Ländern betrieben werden (Anpassungen wie die Lokalisation und
Softwarepatente als Beispiel).

4. Die Verbreitung der mobilen Geräte und der Mobilkommunikation tragen dazu
bei, dass die Entwicklung mehr unsichere Fälle abdecken muss. Dabei sind
unsichere Verbindungen von Datenverbindungen, plötzliches Ausschalten durch
das Risiko mit einem Akkubetrieb. Solche Probleme entstehen erst mit mobilen
Endgeräten, welche zusätzlich betrachtet werden müssen.

5. Viele unterschiedliche Gerätetypen von den verschiedensten Herstellern
müssen unterstützt werden. Zum Beispiel bei Android besonders gut ersichtlich.
Durch die Fragmentierung des Betriebsystems, die vielen unterschiedlichen
Versionen und die Hardwareanforderungen für die extrem vielen Endgeräte
(alleine schon die Unterstützung aller Auflösungen) machen es viel
anspruchsvoller, die Qualität aufrecht zu erhalten.

6. Es gibt heutzutage mehr und mehr abstrakte Softwaresysteme, auch besonders
in der Testsoftware und Softwaremodellierung gibt es viele generische und
Abstrakte Prozesse. Abstrakteres Denken ohne direkte Sicht auf die niedrigeren
Abstraktionsebenen kann die Fehlersuche sehr schwierig. Die Verwendung des
Programms als höhere Abstraktionsschicht, kann sehr unvorhergesehene
Auswirkungen haben

## Aufgabe 2

> Erläutern Sie die Unterschiede zwischen innerer und äußerer Software-Qualität nach ISO 9126 in einer vergleichenden Analyse eines Merkmals für interne und für externe Qualität.

**Innere Qualität** ist die Perspektive der Entwickler oder des gesamten Entwicklungsteams.
Innere Merkmale sind solche Mekrmale, die evaluiert werden können, ohne die Software auszuführen.
Die innere Qualität ist sehr wichtig bei der Softwareanalyse, die kollaborativ bearbeitet werden.
Anpassungen an eine Software wird erleichtert, wenn sie bereits gute Qualität aufweist, da Qualitätsmerkmale wie modulares Programmieren.
Folgenede Qualitätseigenschaften sind auch von esonderer Wichtigkeit für die innere Qualität:
Wartbarkeit, Erweiterbarkeit, Wiederverwendbarkeit, Verständlichkeit und Lesbarkeit des Quellcodes.
Dem Anwender, der die Software ausführt, bleibt dieser Aspekt jedoch verborgen.
Er sieht nur das Resultat in Form von mehr oder weniger zeitnahen oder hochwertigen Anpassungen als Reaktion auf veränderte Anforderungen oder aufgedecktes Fehlverhalten der Software.

**Äußere Qualität** hingegen, ist nicht die Sicht von jemanden der den Quellcode sieht, denn es bezeichnet die Perspektive eines Kunden.
Kunden, welche das Programm benutzen, also ist die Sicht von einem externen Nutzer des Programms in seinem Prozessablauf mit seinen Vorwissen relevant.
Hierbei sind folgende Kriterien besonders wichtig: einfache Verwendbarkeit, Bedien- und Benutzbarkeit, Zuverlässigkeit, Robustheit und Fehlertoleranz.
So ist es für den Anwender von Bedeutung, zum Beispiel durch eine wohlüberlegte Benutzeroberfläche und Benutzerführung in seiner Arbeit mit der Software unterstützt zu werden.
Für den Entwickler, dessen Hauptaugenmerk auf der eigentlichen Funktionalität liegt, ist dieser Aspekt jedoch nachrangig.
Im Falle eines UI / UX Entwicklers muss er die Software zwar dahingehend entwickeln, evaluieren kann er dieses Merkmal abererst, wenn er in die Anwenderrolle wechselt.