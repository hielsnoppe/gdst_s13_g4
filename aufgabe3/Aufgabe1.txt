

Berechnen der zyklomatischen Zahl
=================================

   e = Kanten     = 11
   v = Knoten     = 9
   k = Konstante  = 2

   cv = e  - v + k
      = 11 - 9 + 2
      = 4


Datenflußanalyse
=================

   Vor- und Nachfolger Tabelle
   ===========================


   -------------------------------------
   Anweisung   | Vorgänger | Nachfolger
   -------------------------------------
      6           -           7
      7           6           8
      8           7           9,18
      9           8           10
      10          9           11,13
      11          10          8
      13          10          14,16
      14          13          8
      16          13          8
      18          8           19
      19          18          -


   Datenflußanomalie
   =================

1.  public static final int NOTFOUND = -1;                        // d(NOTFOUND)
2.  // Binaere Suche auf Array a.
3.  // Annahme: a ist sortiert
4.  // Ergebnis: NOTFOUND, wenn k nicht in A enthalten ist.
5.  // Ergebnis: i falls a[i] gleich k ist.
6.  public static int binSearch(int[] a, int k) {                 // d(a,k)
7.    int ug = 0, og = a.length-1, m = 0, pos = NOTFOUND;         // d(ug, og, m, pos), r(a,NOTFOUND)
8.    while (ug <= og && pos == NOTFOUND) {                       // r(ug, og, pos, NOTFOUND)
9.      m = (ug + og) / 2;                                        // d(m), r(ug, og)
10.     if (a[m] == k)                                            // r(a,k)
11.       pos = m;                                                // d(pos),r(m)
12.     else
13.       if (a[m] < k)                                           // r(a,k)
14.          ug = m + 1;                                          // d(ug), r(m)
15.       else
16.          og = m - 1;                                          // d(og), r(m)
17.   }
18.   return pos;                                                 // r(pos)
19. }

